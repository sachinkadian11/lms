declare module "@salesforce/resourceUrl/cal" {
    var cal: string;
    export default cal;
}
declare module "@salesforce/resourceUrl/cal2" {
    var cal2: string;
    export default cal2;
}
declare module "@salesforce/resourceUrl/fullcalendar" {
    var fullcalendar: string;
    export default fullcalendar;
}
declare module "@salesforce/resourceUrl/moment" {
    var moment: string;
    export default moment;
}
declare module "@salesforce/resourceUrl/picker" {
    var picker: string;
    export default picker;
}
