/*
 * Apex controller for lmsHome lwc
 */ 
public class LMS_HomeController {
	@auraEnabled(cacheable=true)
    public static ContactDetailsWrapper getInitialInformation(String userId){
        System.debug(userId);
        ContactDetailsWrapper result = new ContactDetailsWrapper();
        //get the contact from user
        List<Contact> listOfContacts = [select id,Earned_Leaves__c,Earned_Leaves_Availed__c, Sick_Leaves__c ,Sick_Leaves_Availed__c  from Contact where Employee__c =: userId];
        if(listOfContacts!= null && listOfContacts.size()>0){
            List<LeaveHistoryWrapper> listOfLeaveHistories = new List<LeaveHistoryWrapper>();
            for(Time_Off__c  timeOff : [select id,  From_Date__c ,Leave_Type__c ,Number_of_Leaves__c ,Reason__c ,Status__c ,To_Date__c from Time_Off__c where Employee__c =: listOfContacts[0].Id]){
                listOfLeaveHistories.add(new LeaveHistoryWrapper(timeOff));
            }
            result.earnedAvailable = listOfContacts[0].Earned_Leaves__c;
            result.earnedTaken = listOfContacts[0].Earned_Leaves_Availed__c;
            result.sickAvailable = listOfContacts[0].Sick_Leaves__c;
            result.sickTaken = listOfContacts[0].Sick_Leaves_Availed__c;
            result.listOfLeaveHistories = listOfLeaveHistories;
            
        }else{
            throw new AuraHandledException ('Contact not found for user');
        }
        return result;
    }
    
    //creating wrapper classes to hold data so that the solution doesnt depend on any object
    public class ContactDetailsWrapper{
        @auraEnabled public Decimal earnedAvailable;
        @auraEnabled public Decimal earnedTaken;
        @auraEnabled public Decimal sickAvailable;
        @auraEnabled public Decimal sickTaken;
        
        
        @auraEnabled public List<LeaveHistoryWrapper> listOfLeaveHistories;
    }
    public class LeaveHistoryWrapper{
        @auraEnabled public Date startDate;
        @auraEnabled public Date endDate;
        @auraEnabled public String leaveType;
        @auraEnabled public String status;
        @auraEnabled public string comments;
        @auraEnabled public String Id;
        @auraEnabled public String approverName;
        @auraEnabled public String approverId;
        
        public LeaveHistoryWrapper(Time_Off__c timeOff){
            this.startDate = timeOff.From_Date__c;
            this.endDate = timeOff.To_Date__c;
            this.leaveType = timeOff.Leave_Type__c;
            this.status = timeOff.Status__c;
            this.comments = timeOff.Reason__c;
            this.Id = timeOff.Id;
            this.approverName = 'Sachin Kadian';
        }
    }
    
    public class CustomException extends Exception{}

}