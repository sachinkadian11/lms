/*********************************************************/
/********************Import modules***************************/
/*********************************************************/
import { LightningElement,track,wire } from 'lwc';
import userId from '@salesforce/user/Id';
import getUserData from '@salesforce/apex/LMS_HomeController.getInitialInformation';
import { refreshApex } from '@salesforce/apex';
import { CurrentPageReference } from 'lightning/navigation';
import { registerListener, unregisterAllListeners, fireEvent } from 'c/pubsub';

export default class LmsHome extends LightningElement {
    @track currentUserId = userId;
    @track paidLeaveData;
    @track sickLeaveData;
    @track casualLeaveData;
    @track otherLeaveData;
    @track leaveHistoryData;

    
    @wire(CurrentPageReference) pageRef;
    connectedCallback() {
        console.log('currentUserId=='+this.currentUserId);
        registerListener(
            'refreshData',
            this.refreshData,
            this,
        );
    }

    wiredData;
    @wire (getUserData,{userId : '$currentUserId'})
    WireUserData(result){
        //set this variable so that it can be refreshed when cache become stale  
        this.wiredData = result;
        if(result.data){
            console.log('data==',JSON.stringify(result.data));
            //prepare sick leaves 
            this.sickLeaveData = {};
            this.sickLeaveData.leaveType = 'Sick leaves';
            this.sickLeaveData.fillColor="#E64C4C";
            this.sickLeaveData.emptyColor = '#FCE7E7';
            this.sickLeaveData.total = result.data.sickAvailable;
            this.sickLeaveData.taken = result.data.sickTaken;
            this.sickLeaveData.annual = 20;
            

            //prepare paid leaves
            this.paidLeaveData = {};
            this.paidLeaveData.leaveType = 'Paid leaves';
            this.paidLeaveData.fillColor="#25C7A2";
            this.paidLeaveData.emptyColor = '#DEF9E3';
            this.paidLeaveData.total = result.data.earnedAvailable;
            this.paidLeaveData.taken = result.data.earnedTaken;
            this.paidLeaveData.annual = 40;

            //prepare casual leaves
            this.casualLeaveData = {};
            this.casualLeaveData.leaveType = 'Work from Home';
            this.casualLeaveData.fillColor="#f77a52";
            this.casualLeaveData.emptyColor = '#FDDFD5';
            this.casualLeaveData.total = 6;
            this.casualLeaveData.taken = 5;
            this.casualLeaveData.annual = 6;

            //prepare other leaves
            this.otherLeaveData = {};
            this.otherLeaveData.leaveType = 'Other leaves';
            this.otherLeaveData.fillColor="#8E2EF7";
            this.otherLeaveData.emptyColor = '#EAD8FE';
            this.otherLeaveData.total = 10;
            this.otherLeaveData.taken = 0;
            this.otherLeaveData.annual = 15;

            //set leave history data
            this.leaveHistoryData = result.data.listOfLeaveHistories;

        }else if(result.error){
            console.log('lmsHome error==',result.error.details.body.message);
        }
    }

    //refresh data when cache becomes stale or when any update is made in data
    refreshData(){
        refreshApex(this.wiredData);
    }
}