import { LightningElement,wire } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import { fireEvent } from 'c/pubsub';

export default class LmsLeaveActions extends LightningElement {
    @wire(CurrentPageReference) pageRef;
    //this method will fire and event which will be captured by 'lmsNewLeave'
    //lmsNewLeave will catch this event and open the modal
    //third parameter is null, it means we are creating new leave record
    openNewLeaveModal(){
        fireEvent(this.pageRef, 'openNewLeaveModal', null);
    }
}