import { LightningElement } from 'lwc';

import { loadScript, loadStyle } from 'lightning/platformResourceLoader';
import 	fullcalendar from '@salesforce/resourceUrl/fullcalendar';

export default class LmsLeaveCalendar extends LightningElement {

    renderedCallback() {
        Promise.all([
            loadScript(this, fullcalendar + '/fullcalendar/lib/jquery.min.js'),
            loadScript(this, fullcalendar + '/fullcalendar/lib/moment.min.js'),
            loadScript(this, fullcalendar + '/fullcalendar/fullcalendar.js'),
            loadStyle(this, fullcalendar + '/fullcalendar/fullcalendar.css'),
        ]).then(() => {
            console.log('i am trying best');
            $('.cc').addClass('xyz');
            // $('.cc').fullCalendar({
            //     weekends: false,
            //     dayClick: function() {
            //       console.log('a day has been clicked!');
            //     },
            //     defaultView: 'agendaWeek'
                
            // });
            console.log('done');
        })
        
           
    }
}