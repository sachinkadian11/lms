import { LightningElement,api,wire } from 'lwc';
import { fireEvent } from 'c/pubsub';
import { CurrentPageReference } from 'lightning/navigation';

export default class LookupItem extends LightningElement {
    @api item;

    @wire(CurrentPageReference) pageRef;

    //function is called when user selects any record
    selectRecord(){
        fireEvent(this.pageRef, 'lookupValueSelected', this.item);
    }
}