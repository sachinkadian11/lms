import { LightningElement,api,track } from 'lwc';

export default class LmsLeaveCard extends LightningElement {
    innerRadius = 70;
    outerRadius = 83;
    @api leaveData;

    connectedCallback() {
        let tempLeaveData = {};
        tempLeaveData.leaveType = 'Leave Type';
        tempLeaveData.total = 0;
        tempLeaveData.taken = 0;
        this.leaveData = tempLeaveData;
    }
    
    get leaveBalance(){
        return this.leaveData.total - this.leaveData.taken;
    }

    get dashOffset(){
        return (2* this.innerRadius * 3.14) * (1-this.leaveBalance/this.leaveData.total);
    }

    get dashArray(){
        return 2*this.innerRadius*3.14;
    }
    
}