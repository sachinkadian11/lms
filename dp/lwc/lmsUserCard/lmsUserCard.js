import { LightningElement,api, wire } from 'lwc';
import { getRecord } from 'lightning/uiRecordApi';

const fields = [
    'User.SmallPhotoUrl',
    'User.FullPhotoUrl',
    'User.Name',
    'User.Email'
];

export default class LmsUserCard extends LightningElement {
    @api currentUserId;
    @api label;
    smallPhotoURL;

    //get small photo url from user object
    @wire(getRecord, {recordId : '0059A000001CjToQAK',fields})
    currentUser;

    get userPhotoURL(){
        return this.currentUser.data.fields.FullPhotoUrl.value;
    }

}