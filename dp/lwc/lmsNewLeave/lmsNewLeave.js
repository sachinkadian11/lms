import { LightningElement, wire } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import { registerListener, unregisterAllListeners, fireEvent } from 'c/pubsub';

import TIMEOFF_OBJECT from '@salesforce/schema/Time_Off__c';
import FROM_FIELD from '@salesforce/schema/Time_Off__c.From_Date__c';
import TO_FIELD from '@salesforce/schema/Time_Off__c.To_Date__c';
import REASON_FIELD from '@salesforce/schema/Time_Off__c.Reason__c';
import TYPE_FIELD from '@salesforce/schema/Time_Off__c.Leave_Type__c';

export default class LmsNewLeave extends LightningElement {

    timeOffFields = [FROM_FIELD, TO_FIELD, REASON_FIELD, TYPE_FIELD];
    timeOffObject = TIMEOFF_OBJECT;

    //register event
    //this method will open modal when event is captured
    @wire(CurrentPageReference) pageRef;
    connectedCallback() {
        registerListener(
            'openNewLeaveModal',
            this.openModal,
            this,
        );
    }

    //this method will open new/edit leave modal
    openModal() {
        let modal = this.template.querySelector('.new-leave-modal');
        let backdrop = this.template.querySelector('.add-new-backdrop');
        modal.classList.add('slds-fade-in-open');
        backdrop.classList.add('slds-backdrop_open');
    }
    //this method will close new/edit leave modal
    closeModal() {
        let modal = this.template.querySelector('.new-leave-modal');
        let backdrop = this.template.querySelector('.add-new-backdrop');
        modal.classList.remove('slds-fade-in-open');
        backdrop.classList.remove('slds-backdrop_open');
    }
    //method called when new timeoff is created
    handleSubmit(evt) {
        //stop the event so that additional fields can be added to this
        evt.preventDefault(); 
        const fields = evt.detail.fields;
        fields.Employee__c = '0039A000002titeQAA';
        fields.Status__c = 'Pending';
        this.template.querySelector('.record-edit-form').submit(fields);
    }

    //method called when record is successfully saved
    handleSuccess(event){
        //fire the event once record is saved successfully
        //this event will be captured by lmsHome
        //lmsHome will capture it and refesh the data so that new record can be reflected there
        fireEvent(this.pageRef, 'refreshData', null);
        this.closeModal();
    }
    //method called when there is some error while saving record
    hanldeError(error){
        console.log(error.errorCode);
        console.log(error.message);
    }
}