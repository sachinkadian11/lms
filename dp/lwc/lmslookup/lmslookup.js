import { LightningElement,api, wire ,track} from 'lwc';
import getLookupValues from '@salesforce/apex/LMS_LookupController.fetchLookUpValues';
import { CurrentPageReference } from 'lightning/navigation';
import { registerListener, unregisterAllListeners, fireEvent } from 'c/pubsub';

/** The delay used when debouncing event handlers before invoking Apex. */
const DELAY = 300;

export default class Lmslookup extends LightningElement {
    @api objName;
    @track searchKey;
    @track listOfResults;
    @track errorMessage;
    @track selectedRecord;

    connectedCallback() {
        registerListener(
            'lookupValueSelected',
            this.selectRecord,
            this,
        );
    }

    @wire(CurrentPageReference) pageRef

    @wire(getLookupValues,{searchKeyWord : '$searchKey',ObjectName : '$objName'})
    wiredValues({error,data}){
        if(data){
            this.listOfResults = data;
            //open search results
            if(data.length > 0){
                this.openResults();
            }
        }else if(error){
            this.errorMessage = error;
            this.closeResults();
        }
    }

    //when search tem is changed, this method change 'searchKey'
    changeSearchValues(event){
        window.clearTimeout(this.delayTimeout);
        const searchKey = event.target.value;
        this.delayTimeout = setTimeout(() => {
            this.searchKey = searchKey;
        }, DELAY);
    }

    selectRecord(record){
        this.selectedRecord = record;
        this.template.querySelector('.searchBox').classList.add('slds-hide');
        this.closeResults();
        const changedValueEvt = new CustomEvent('lookupchanged', { detail: this.selectedRecord.Id });
        this.dispatchEvent(changedValueEvt);
    }

    removeSelected(){
        this.selectedRecord  = null;
        this.template.querySelector('.searchBox').classList.remove('slds-hide');
        this.openResults();
        const changedValueEvt = new CustomEvent('lookupchanged', { detail: null});
        this.dispatchEvent(changedValueEvt);
    }

    //when blur happens on parent div
    onblur(){
        //this.closeResults();
    }

    //calls when user clicks on cross icon in search textbox
    clearResults(){
        console.log('called');
    }

    //open the results list
    openResults(){
        this.template.querySelector('.searchDiv').classList.add('slds-is-open');
        this.template.querySelector('.searchDiv').classList.remove('slds-is-close');
    }

    //close the results list
    closeResults(){
        this.template.querySelector('.searchDiv').classList.add('slds-is-close');
        this.template.querySelector('.searchDiv').classList.remove('slds-is-open');
    }
}