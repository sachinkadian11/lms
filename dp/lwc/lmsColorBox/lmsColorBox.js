import { LightningElement,api } from 'lwc';

export default class LmsColorBox extends LightningElement {
    @api color;

    get spanStyle(){
        return "background:"+this.color;
    }
}