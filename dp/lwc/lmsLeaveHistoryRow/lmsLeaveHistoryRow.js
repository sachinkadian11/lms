import { LightningElement,api } from 'lwc';

export default class LmsLeaveHistoryRow extends LightningElement {
    @api leave;

    //change the color of status 
    get style(){
        if(this.leave.status == 'Approved'){
            return 'color:#25C7A2'
        }else if(this.leave.status == 'Pending'){
            return 'color:#f77a52';
        }else{
            return 'color:#E64C4C'
            
        }
    }
}